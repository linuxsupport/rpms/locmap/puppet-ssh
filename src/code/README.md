# puppet-ssh  

This ssh module is designed to install, enable and configure
ssh client part.  
### Parameters to ssh class
* `loglevel`Gives the verbosity level that is used when logging messages from ssh, default is VERBOSE, other values are   QUIET, FATAL, ERROR, INFO, DEBUG, DEBUG1, DEBUG2, and DEBUG3  
* `permit_root_login`Specifies whether root can	log in using ssh. default is yes, other values are prohibit-password, without-password, forced-commands-only, or no
* `port`Specifies the port	number that sshd listens on, the default is 22  
*locmap populated values*
* `foundkey` If locmap finds a ssh key for repsonsible user change it to 'true', default is false
* `keytype` Ssh key type, default to undef, other values are 'ssh-rsa', 'ssh-dsa'
* `renspubkey` Renponsible user ssh public key,( i.e AAAAB3NzaC1yc2....XSw== ), default is undef
* `fullname` Full name of key owner (i.e user@something.cern.ch), default is undef  

Puppet databindings allows all the above settings to be set via hiera, users can override the default values inside '/etc/puppet/userdata/module_names/ssh/ssh.yaml'  

```YAML
---
port:2222
permit_root_login:'no'
```  

## Contact
Aris Boutselis <aris.boutselis@cern.ch>  

## Support
https://gitlab.cern.ch/linuxsupport/puppet-ssh


