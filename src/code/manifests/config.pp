class ssh::config {

  file { 'sshd_config':
    ensure  => present,
    path    => $ssh::params::sshd_config_file,
    owner   => $ssh::params::config_file_owner,
    group   => $ssh::params::config_file_group,
    mode    => $ssh::params::sshd_config_file_mode,
    content => template($ssh::params::sshd_template),
    notify  => Class['ssh::service'],
  }

  file { 'ssh_config':
    ensure  => present,
    path    => $ssh::params::ssh_config_file,
    owner   => $ssh::params::config_file_owner,
    group   => $ssh::params::config_file_group,
    mode    => $ssh::params::ssh_config_file_mode,
    content => template($ssh::params::ssh_template),
  }

  $ssh::params::public_keys.each|$index, String $ssh_key| {
    if 'SSH:' in $ssh_key {
      $key = split($ssh_key,':')
      $sanekey = split($key[1], ' ')
      if $sanekey[0] in [ 'ssh-dss', 'ssh-rsa', 'ecdsa-sha2-nistp256', 'ecdsa-sha2-nistp384', 'ecdsa-sha2-nistp521', 'ssh-ed25519' ] {
        ssh_authorized_key { "ssh key from ldap $index":
          user => 'root',
          type => $sanekey[0],
          key  => $sanekey[1]
        }
      }
    }
  }
}
