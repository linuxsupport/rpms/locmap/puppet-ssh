class ssh {

  class {'ssh::params':}
  class {'ssh::install':}
  class {'ssh::service':}
  class {'ssh::config':}

}
