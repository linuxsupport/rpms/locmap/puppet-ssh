class ssh::install {

  package { $ssh::params::ssh_pkgs:
    ensure => 'present',
    before => Class['ssh::config'],
  }
}
