class ssh::params {
  $ssh_pkgs                        = ['openssh-clients', 'openssh-server']
  $service_name                    = 'sshd'
  $config_file_group               = 'root'
  $config_file_owner               = 'root'
  $authorized_keys_user            = 'root'
  $authorized_keys_target          = '/root/.ssh/authorized_keys'
  $sshd_config_file                = '/etc/ssh/sshd_config'
  $sshd_config_file_mode           = '0600'
  $ssh_config_file                 = '/etc/ssh/ssh_config'
  $ssh_config_file_mode            = '0644'
  $sshd_template                   = 'ssh/sshd_config.erb'
  $ssh_template                    = 'ssh/ssh_config.erb'
  $ssh_authkeys_template           = 'ssh/authorized_keys.erb'
# Parameters for the client configuration file /etc/ssh/ssh_config
  $gssapiauthentication            = 'yes'
  $gssapidelegatecredentials       = 'yes'
  $gssapitrustdns                  = 'yes'
# Parameters for the daemon configuration file /etc/ssh/sshd_config
  $loglevel                        = lookup({"name" => "loglevel", "default_value" => 'VERBOSE'})
  $challengeresponseauthentication = 'yes'
  $usePAM                          = 'yes'
  $permitrootlogin                 = lookup({"name" => "permit_root_login", "default_value" => 'yes'})
  $port                            = lookup({"name" => "port", "default_value" => '22'})
# Public key(s), foundkey
  $public_keys                     = lookup({"name" => "public_keys", "default_value" => []})
}
