Name:		puppet-ssh
Version:	2.5
Release:	1%{?dist}
Summary:	Masterless puppet module for ssh

Group:		CERN/Utilities
License:	BSD
URL:		http://linux.cern.ch
Source0:	%{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for ssh.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/ssh/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/ssh/
touch %{buildroot}/%{_datadir}/puppet/modules/ssh/linuxsupport

%files -n puppet-ssh
%{_datadir}/puppet/modules/ssh
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.5-1
- Add autoreconfigure %post script

* Thu Apr 18 2024 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> 2.4-1
- Disable GSSAPICleanupCredentials

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.3-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.3-2
- fix requires on puppet-agent

* Fri Feb 19 2021 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- fix ssh key deployment

* Fri Jan 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- fix deprecation warnings related to hiera lookups

* Fri Jan 10 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- add support for multiple ssh keys from ldap

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- build for el8

* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Fri Dec 16 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.3-1
- Missing directory creation for /root/.ssh :(

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
